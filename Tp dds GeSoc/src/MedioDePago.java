public class MedioDePago {

    private String medioDePago;
    private String payment_typeId;
    private String id;

    public String getMedioDePago() {
        return medioDePago;
    }

    public String getPayment_typeId() {
        return payment_typeId;
    }

    public String getId() {
        return id;
    }

    public MedioDePago(String id) {
        this.id = id;
    }

    //habria que hacer un json o algo con la tabla de medios de pago de mercado libre, asi con solo poner el id se completan los datos//

}

