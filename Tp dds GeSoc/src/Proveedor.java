public class Proveedor {

    private String nombre;
    private String apellido;
    private Number dni;
    private String razonSocial;
    private Number cuil;
    private Number cuit;
    private String direccionPostal;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Number getDni() {
        return dni;
    }

    public void setDni(Number dni) {
        this.dni = dni;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public Number getCuil() {
        return cuil;
    }

    public void setCuil(Number cuil) {
        this.cuil = cuil;
    }

    public Number getCuit() {
        return cuit;
    }

    public void setCuit(Number cuit) {
        this.cuit = cuit;
    }

    public String getDireccionPostal() {
        return direccionPostal;
    }

    public void setDireccionPostal(String direccionPostal) {
        this.direccionPostal = direccionPostal;
    }

    public void cargarProveedorEmpresa( String nuevaRazonSocial, Number nuevoCuit,String nuevaDireccionPostal  ){

        this.setRazonSocial(nuevaRazonSocial);
        this.setCuit(nuevoCuit);
        this.setDireccionPostal(nuevaDireccionPostal);
    }

    public void cargarProveedorPersona( String nuevoNombre,String nuevoApellido,Number dni,String nuevaDireccionPostal   ){

        this.setNombre(nuevoNombre);
        this.setApellido(nuevoApellido);
        this.setDni(dni);
        this.setDireccionPostal(nuevaDireccionPostal);
    }

    //medio raro pero que se yo//
}
