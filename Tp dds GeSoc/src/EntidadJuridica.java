public class EntidadJuridica {

    private String razonSocial;
    private String nombre;
    private String cuit;
    private String direccion;
    private Integer codInscripcionIgj;
    private EntidadBase entidadBase;

    public EntidadBase getEntidadBase() {
        return entidadBase;
    }

    public void setEntidadBase(EntidadBase entidadBase) {
        this.entidadBase = entidadBase;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public String getNombre() {
        return nombre;
    }

    public String getCuit() {
        return cuit;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Integer getCodInscripcionIgj() {
        return codInscripcionIgj;
    }

    public EntidadJuridica(String razonSocial, String nombre, String cuit, Integer codInscripcionIgj) {
        this.razonSocial = razonSocial;
        this.nombre = nombre;
        this.cuit = cuit;
        this.codInscripcionIgj = codInscripcionIgj;
    }
}
