import java.awt.*;

public class DocumentoComercial {

    private TipoDocComercial tipoDoccomercial;
    private String numero;
    private String enlaceAdjunto;
    private Image adjunto;

    public String getEnlaceAdjunto() {
        return enlaceAdjunto;
    }

    public void setEnlaceAdjunto(String enlaceAdjunto) {
        this.enlaceAdjunto = enlaceAdjunto;
    }

    public Image getAdjunto() {
        return adjunto;
    }

    public void setAdjunto(Image adjunto) {
        this.adjunto = adjunto;
    }

    public TipoDocComercial getTipoDoccomercial() {
        return tipoDoccomercial;
    }

    public String getNumero() {
        return numero;
    }

    public DocumentoComercial(TipoDocComercial tipoDoccomercial, String numero) {
        this.tipoDoccomercial = tipoDoccomercial;
        this.numero = numero;
    }

    // ver si deberia llevar los datos de la operacion, ej: lista de items, fecha, etc//

    protected enum TipoDocComercial{
        factura, ticket, recibo
    }
}

