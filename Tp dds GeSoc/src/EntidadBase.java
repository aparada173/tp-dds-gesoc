public class EntidadBase {

    private String nombre;
    private String detalle;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public EntidadBase(String nombre) {
        this.nombre = nombre;
    }
    //ver lo de estar asosiada a sol ouna entidad juridica//

}
