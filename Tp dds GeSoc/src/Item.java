import java.text.DecimalFormat;

public class Item {

    private String detalle;
    private String descripcion;
    private int valor;

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public Item(String detalle, String descripcion, int valor) {
        this.detalle = detalle;
        this.descripcion = descripcion;
        this.valor = valor;
    }
}

