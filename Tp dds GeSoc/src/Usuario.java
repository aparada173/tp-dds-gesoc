public class Usuario {

    private TipoDeUsuario tipoDeUsuario;
    private String nombreUsuario;
    private String contraseña;

    public TipoDeUsuario getTipoDeUsuario() {
        return tipoDeUsuario;
    }

    public void setTipoDeUsuario(TipoDeUsuario tipoDeUsuario) {
        this.tipoDeUsuario = tipoDeUsuario;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getContraseña() {
        return contraseña;
    }


    public Usuario(TipoDeUsuario tipoDeUsuario, String nombreUsuario, String contraseña) {
        this.validarContraseña(contraseña);
        this.tipoDeUsuario = tipoDeUsuario;
        this.nombreUsuario = nombreUsuario;
        this.contraseña = contraseña;
        this.registrarse();
    }

    public Boolean validarContraseña( String contraseña){

      return  ValidadorContraseña.EsValida(contraseña);
        //validador es la instancia que va a hacer la verificacion de contraseña segura//
    }

    public void registrarse(){

        GestorEgreso.registrarUsuario(this);

    }


    public enum TipoDeUsuario {
       administrador,
        estandar;
    }

}
