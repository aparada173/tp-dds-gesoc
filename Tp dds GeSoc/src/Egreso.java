import sun.util.calendar.BaseCalendar;
import java.util.List;

public class Egreso {

    private BaseCalendar.Date fecha;
    private Proveedor proveedor;
    private MedioDePago medioDePago;
    private DocumentoComercial docComercial;
    private EntidadJuridica datosPropios; //lo dejo como entidad juridia y despues veo si lo diferencio de las bases//
    private List<Item> items;


    public BaseCalendar.Date getFecha() {
        return fecha;
    }

    public void setFecha(BaseCalendar.Date fecha) {
        this.fecha = fecha;
    }

    public Proveedor getProveedor() {
        return proveedor;
    }

    public void setProveedor(Proveedor proveedor) {
        this.proveedor = proveedor;
    }

    public MedioDePago getMedioDePago() {
        return medioDePago;
    }

    public void setMedioDePago(MedioDePago medioDePago) {
        this.medioDePago = medioDePago;
    }

    public DocumentoComercial getDocComercial() {
        return docComercial;
    }

    public void setDocComercial(DocumentoComercial docComercial) {
        this.docComercial = docComercial;
    }

    public EntidadJuridica getDatosPropios() {
        return datosPropios;
    }

    public void setDatosPropios(EntidadJuridica datosPropios) {
        this.datosPropios = datosPropios;
    }

    public Egreso(Proveedor proveedor, MedioDePago medioDePago, List<Item> items) {
        this.proveedor = proveedor;
        this.medioDePago = medioDePago;
        this.items = items;
    }

    public double valorTotalDeLaOperacion(){

        return this.items.stream().mapToInt( item -> item.getValor() ).sum();
    }
    public void adjuntarDocumentoComercial(DocumentoComercial docComercial){

        this.setDocComercial(docComercial);

    }



}

